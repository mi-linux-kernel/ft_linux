# ft_linux

This is not about Kernel programming, but it’s highly related.
This distro will be the base for all your kernel projects, because all your kernel-code will
be executed here, on your distro.
Try to implement what you want/need to. This is your userspace, take care of it!

# Goals
1) Build a Linux Kernel.
2) Install some binaries.
3) Implement a filesystem hierarchy compliant with the standards.
4) Connect to the Internet.

This distro will be the base for all my kernel projects, because all my
kernel-code will be executed here, on my distro.
